#!/usr/bin/env python 
'''
print_win32_vector.py
This extension will generate vector graphics printout, specifically for Windows GDI32.

Copyright (C) 2012 Alvin Penner, penner@vaxxine.com (path, rect, and default printer support)
              2017 Art McBain, mcbain.asm@gmail.com (printer choice, settings, and improved SVG support)

This is a modified version of the file dxf_outlines.py by Aaron Spike, aaron@ekips.org

- see http://www.lessanvaezi.com/changing-printer-settings-using-the-windows-api/
- get GdiPrintSample.zip at http://archive.msdn.microsoft.com/WindowsPrintSample

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
# standard library
from ctypes import *
from ctypes import util
from ctypes.wintypes import *
import re
from copy import deepcopy
from time import sleep
from subprocess import Popen, PIPE
from shutil import copy2
import os
from lxml import etree

# local library
import inkex
import simplestyle
import simpletransform
import cubicsuperpath

inkex.localize()                        # Initialize gettext
if not inkex.sys.platform.startswith('win'):
    exit(_('sorry, this will run only on Windows, exiting...'))

myspool = WinDLL('winspool.drv')
mycomdlg = WinDLL('Comdlg32.dll')
mykernel = WinDLL('Kernel32.dll')
mygdi = WinDLL('gdi32.dll')
DM_IN_PROMPT = 4                        # call printer property sheet 
DM_OUT_BUFFER = 2                       # write to DEVMODE structure

# Windows API constant defines

# PolygonFillModes; https://msdn.microsoft.com/en-us/library/cc230540.aspx
ALTERNATE = 1
WINDING = 2

# Brush styles; https://msdn.microsoft.com/en-us/library/cc250411.aspx
BS_SOLID = 0

# Pen styles; https://msdn.microsoft.com/en-us/library/cc231188.aspx
PS_ENDCAP_ROUND = 0x00000000
PS_JOIN_ROUND = 0x00000000
PS_SOLID = 0x00000000
PS_ENDCAP_SQUARE = 0x00000100
PS_ENDCAP_FLAT = 0x00000200
PS_JOIN_BEVEL = 0x00001000
PS_JOIN_MITER = 0x00002000
PS_GEOMETRIC = 0x00010000

# PRINTDLG Flags; https://msdn.microsoft.com/en-us/library/windows/desktop/ms646843(v=vs.85).aspx
PD_NONETWORKBUTTON = 0x00200000
PD_PRINTSETUP = 0x00000040
PD_RETURNDC = 0x00000100

printErrors = dict()
printErrors[0xFFFF] = 'CDERR_DIALOGFAILURE'
printErrors[0x0006] = 'CDERR_FINDRESFAILURE'
printErrors[0x0002] = 'CDERR_INITIALIZATION'
printErrors[0x0007] = 'CDERR_LOADRESFAILURE'
printErrors[0x0005] = 'CDERR_LOADSTRFAILURE'
printErrors[0x0008] = 'CDERR_LOCKRESFAILURE'
printErrors[0x0009] = 'CDERR_MEMALLOCFAILURE'
printErrors[0x000A] = 'CDERR_MEMLOCKFAILURE'
printErrors[0x0004] = 'CDERR_NOHINSTANCE'
printErrors[0x000B] = 'CDERR_NOHOOK'
printErrors[0x0003] = 'CDERR_NOTEMPLATE'
printErrors[0x000C] = 'CDERR_REGISTERMSGFAIL'
printErrors[0x0001] = 'CDERR_STRUCTSIZE'
printErrors[0x100A] = 'PDERR_CREATEICFAILURE'
printErrors[0x100C] = 'PDERR_DEFAULTDIFFERENT'
printErrors[0x1009] = 'PDERR_DNDMMISMATCH'
printErrors[0x1005] = 'PDERR_GETDEVMODEFAIL'
printErrors[0x1006] = 'PDERR_INITFAILURE'
printErrors[0x1004] = 'PDERR_LOADDRVFAILURE'
printErrors[0x1008] = 'PDERR_NODEFAULTPRN'
printErrors[0x1007] = 'PDERR_NODEVICES'
printErrors[0x1002] = 'PDERR_PARSEFAILURE'
printErrors[0x100B] = 'PDERR_PRINTERNOTFOUND'
printErrors[0x1003] = 'PDERR_RETDEFFAILURE'
printErrors[0x1001] = 'PDERR_SETUPFAILURE'

# OK, seriously? The Windows API sucks.
class POINTL(Structure):
    _fields_ = [('x', c_long),
                ('y', c_long)]

class dmStruct1(Structure):
    _fields_ = [('dmOrientation', c_short),
                ('dmPaperSize', c_short),
                ('dmPaperLength', c_short),
                ('dmPaperWidth', c_short),
                ('dmScale', c_short),
                ('dmCopies', c_short),
                ('dmDefaultSource', c_short),
                ('dmPrintQuality', c_short)]

class dmStruct2(Structure):
    _fields_ = [('dmPosition', POINTL),
                ('dmDisplayOrientation', DWORD),
                ('dmDisplayFixedOutput', DWORD)]

class dmUnion1(Union):
    _anonymous_ = ('struct1', 'struct2')
    _fields_ = [('struct1', dmStruct1),
                ('struct2', dmStruct2)]

class dmUnion2(Union):
    _fields_ = [('dmDisplayFlags', DWORD),
                ('dmNup', DWORD)]

class DEVMODE(Structure):
    _anonymous_ = ('union1', 'union2')
    _fields_ = [('dmDeviceName', c_char*32),
                ('dmSpecVersion', WORD),
                ('dmDriverVersion', WORD),
                ('dmSize', WORD),
                ('dmDriverExtra', WORD),
                ('dmFields', DWORD),
                ('union1', dmUnion1),
                ('dmColor', c_short),
                ('dmDuplex', c_short),
                ('dmYResolution', c_short),
                ('dmTTOption', c_short),
                ('dmCollate', c_short),
                ('dmFormName', c_char*32),
                ('dmLogPixels', WORD),
                ('dmBitsPerPel', DWORD),
                ('dmPelsWidth', DWORD),
                ('dmPelsHeight', DWORD),
                ('union2', dmUnion2),
                ('dmDisplayFrequency', DWORD),
                ('dmICMMethod', DWORD),
                ('dmICMIntent', DWORD),
                ('dmMediaType', DWORD),
                ('dmDitherType', DWORD),
                ('dmReserved1', DWORD),
                ('dmReserved2', DWORD),
                ('dmPanningWidth', DWORD),
                ('dmPanningHeight', DWORD)]

class DEVNAMES(Structure):
    _fields_ = [('wDriverOffset', WORD),
                ('wDeviceOffset', WORD),
                ('wOutputOffset', WORD),
                ('wDefault', WORD)]

class PRINTDLG(Structure):
    _fields_ = [('lStructSize', DWORD),
                ('hwndOwner', c_void_p),
                ('hDevMode', POINTER(DEVMODE)),
                ('hDevNames', POINTER(DEVNAMES)),
                ('hDC', c_void_p),
                ('Flags', DWORD),
                ('nFromPage', WORD),
                ('nToPage', WORD),
                ('nMinPage', WORD),
                ('nMaxPage', WORD),
                ('nCopies', WORD),
                ('hInstance', c_void_p),
                ('lCustData', POINTER(c_long)),
                ('lpfnPrintHook', POINTER(c_long)),
                ('lpfnSetupHook', POINTER(c_long)),
                ('lpPrintTemplateName', LPCSTR),
                ('lpSetupTemplateName', LPCSTR),
                ('hPrintTemplate', c_void_p),
                ('hSetupTemplate', c_void_p)]

    def __init__(self, flags):
        # Do not preallocate hDevMode or hDevNames here. The print dialog will crash upon user accept.
        # When passed in as NULL, the system will create them for us but we're required to free them.
        # Seeing as we can just load libc and call free() there's no point to making our structs work.
        self.Flags = c_ulong(flags)
        self.lStructSize = c_ulong(sizeof(PRINTDLG))

class DOCINFO(Structure):
    _fields_ = [('cbSize', c_int),
                ('lpszDocName', LPCSTR),
                ('lpszOutput', LPCSTR),
                ('lpszDatatype', LPCSTR),
                ('fwType', DWORD)]

    def __init__(self, name):
        self.lpszDocName = LPCSTR(name)
        self.lpszOutput = LPCSTR()
        self.lpszDatatype = LPCSTR()
        self.fwType = DWORD(0)
        self.cbSize = c_int(sizeof(DOCINFO))

class LOGBRUSH(Structure):
    _fields_ = [('lbStyle', c_uint),
                ('lbColor', DWORD),
                ('lbHatch', POINTER(c_ulong))]

    def __init__(self, style, color, hatch=None):
        self.lbStyle = c_uint(style)
        self.lbColor = DWORD(color)
        if hatch is not None:
            self.lbHatch = POINTER(c_ulong)(c_ulong(hatch))
        else:
            self.lbHatch = POINTER(c_ulong)()

class Win32VectorPrinter(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        # Inkscape requires we have an option called "tab" here just to have tabs in the UI defined in the INX file it seems.
        self.OptionParser.add_option('--tab',                   action='store', type='string',  dest='tab',                                  help='unused, ignore')
        self.OptionParser.add_option('-j', '--jobType',         action='store', type='string',  dest='jobType',         default='combined',  help='combined, compatibility, raster, vector')
        self.OptionParser.add_option('-t', '--vectorThreshold', action='store', type='float',   dest='vectorThreshold', default=.25,         help='Maximum value for strokes to still be rendered as scaleless "vector" lines')
        self.OptionParser.add_option('-a', '--autoConvertText', action='store', type='string',  dest='autoConvertText', default='true',      help='Automatically convert text to paths; default is true')
        self.visibleLayers = True       # print only visible layers

    def process_shape(self, node, mat, fillmode):
        color = None                    # stroke color
        fillcolor = None                # fill color
        hasStroke = False               # pre-int() test of stroke width
        stroke = 1                      # pen width in printer pixels
        strokeEndCap = PS_ENDCAP_FLAT   # butt (flat), round, square
        strokeLineJoin = PS_JOIN_MITER  # round, bevel, miter
        miterLimit = 4                  # SVG's default value (spec)
        pf = None                       # fill path
        emitter = self.emit_path
        jobType = self.options.jobType

        # Very NB : If the pen width is greater than 1 then the output will Not be a vector output !
        style = node.get('style')
        if style:
            style = simplestyle.parseStyle(style)

            if style.has_key('display') and style['display'] == 'none':
                return

            if style.has_key('fill-rule'):
                rule = style['fill-rule']
                if rule != 'inherit' and (rule == 'nonzero' or rule == 'evenodd'):
                    fillmode = rule

            if style.has_key('stroke'):
                if style['stroke'] and style['stroke'] != 'none' and style['stroke'][0:3] != 'url':
                    color = simplestyle.parseColor(style['stroke'])
                    color = color[0] + 256*color[1] + 256*256*color[2]

            if style.has_key('stroke-width'):
                stroke = self.unittouu(style['stroke-width'])/self.unittouu('1px')
                hasStroke = stroke > 0

            if style.has_key('fill'):
                # Warn on finding an unsupported fill type of 'url' here?
                if style['fill'] and style['fill'] != 'none' and style['fill'][0:3] != 'url':
                    fill = simplestyle.parseColor(style['fill'])
                    fillcolor = fill[0] + 256*fill[1] + 256*256*fill[2]
            else:
                fillcolor = 0

            linecap = None
            if style.has_key('stroke-linecap'):
                linecap = style['stroke-linecap']
                if linecap == 'round':
                    strokeEndCap = PS_ENDCAP_ROUND
                elif linecap == 'square':
                    strokeEndCap = PS_ENDCAP_SQUARE

            if style.has_key('stroke-linejoin'):
                linejoin = style['stroke-linejoin']
                if linejoin == 'round':
                    strokeLineJoin = PS_JOIN_ROUND
                if linejoin == 'bevel':
                    strokeLineJoin = PS_JOIN_BEVEL

            # The GDI API takes a float here, for once, but must be no less than 1.
            if style.has_key('stroke-miterlimit'):
                miterLimit = max(1.0, float(style['stroke-miterlimit']))
        else:
            fillcolor = 0

        if node.tag == inkex.addNS('path','svg'):
            d = node.get('d')
            if not d:
                return
            p = cubicsuperpath.parsePath(d)

        elif node.tag == inkex.addNS('rect','svg'):
            x = float(node.get('x'))
            y = float(node.get('y'))
            width = float(node.get('width'))
            height = float(node.get('height'))
            rx = node.get('rx')
            ry = node.get('ry')
            rxf = float(rx) if rx is not None else 0
            ryf = float(ry) if ry is not None else 0

            if rxf < 0:
                rx = None
                rxf = 0

            if ryf < 0:
                ry = None
                ryf = 0

            if (rx is not None and rx > 0) or (ry is not None and ry > 0):
                if rxf is None or rxf == 0:
                    rx = ry
                    rxf = ryf
                elif ry is None or ryf == 0:
                    ry = rx
                    ryf = rxf

                halfW = width / 2
                if rxf > halfW:
                    rx = str(halfW)
                    rxf = halfW

                halfH = height / 2
                if ryf > halfH:
                    ry = str(halfH)
                    ryf = halfH

                edgeX = str(width - rxf * 2)
                edgeY = str(height - ryf * 2)

                # Same as below for circles and ellipses... could call ArcToPath directly?
                p = cubicsuperpath.parsePath('M' + str(x + rxf) + ' ' + str(y)
                        + ' h' + edgeX + ' a ' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' ' + ry
                        + ' v' + edgeY + ' a ' + rx + ' ' + ry + ' 0 0 1 -' + rx + ' ' + ry
                        + ' h-' + edgeX + ' a ' + rx + ' ' + ry + ' 0 0 1 -' + rx + ' -' + ry
                        + ' v-' + edgeY + ' a ' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' -' + ry)
            else:
                p = [[[[x, y]], [[x + width, y]], [[x + width, y + height]], [[x, y + height]]]]
                p = [self.fix_end_gaps(p[0])]
                emitter = self.emit_lines

        elif node.tag in [inkex.addNS('circle','svg'), inkex.addNS('ellipse','svg')]:
            cx = node.get('cx')
            cy = node.get('cy')

            if node.tag == inkex.addNS('circle','svg'):
                rx = node.get('r')
                ry = rx
            else:
                rx = node.get('rx')
                ry = node.get('ry')
            # Could (should?) call ArcToPath directly and append the second to fourth arcs to the output of the first
            p = cubicsuperpath.parsePath('M' + cx + ' ' + str(float(cy) - float(ry)) + ' a ' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' ' + ry
                    + 'a ' + rx + ' ' + ry + ' 0 0 1 -' + rx + ' ' + ry
                    + 'a ' + rx + ' ' + ry + ' 0 0 1 -' + rx + ' -' + ry
                    + 'a ' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' -' + ry + 'z')

        elif node.tag == inkex.addNS('line','svg'):
            p = [[[[float(node.get('x1')), float(node.get('y1'))]], [[float(node.get('x2')), float(node.get('y2'))]]]]
            emitter = self.emit_lines

        elif node.tag in [inkex.addNS('polygon','svg'), inkex.addNS('polyline','svg')]:
            points = node.get('points')
            if not points:
                return

            # Overkill? Could something be faster than a regex here if we're worried about export performance?
            points = re.split('\s+|,', points.strip())
            polygon = node.tag == inkex.addNS('polygon','svg')
            p = []

            for i in range(0, len(points), 2):
                p.append([[float(points[i]), float(points[i + 1])]])

            if polygon:
                p = self.fix_end_gaps(p)
            elif fillcolor:
                p.append([[p[0][0][0], p[0][0][1]]])

            if not polygon and fillcolor:
                pf = [deepcopy(p)]
                p = p[:-1]

            p = [p]
            emitter = self.emit_lines

        # If flowRoots are detected on Effect start they're converted to paths. Therefore we shouldn't (in theory) see
        # this message. However the user should know their print won't be right if we ever hit this trap.
        elif node.tag in [inkex.addNS('flowRoot','svg'), inkex.addNS('text','svg')] and self.options.autoConvertText == 'true':
            inkex.debug('Well this is awkward. We forgot to turn text object #' + node.get('id')
                + ' into a path before processing and must skip it.')
            return
        else:
            return

        trans = node.get('transform')
        if trans:
            mat = simpletransform.composeTransform(mat, simpletransform.parseTransform(trans))
        simpletransform.applyTransformToPath(mat, p)

        if pf is not None:
            simpletransform.applyTransformToPath(mat, pf)

        # Shortcut scale transform :) The other parts zero themselves out.
        stroke = stroke * abs(mat[0][0])

        # This must be done after all transforms are applied. If it isn't, the difference between the value before all
        # transforms and the value of the user set vector threshold may result in some tags printing their stroke when
        # doing a raster only job. self.vectorThreshold is in device units for accurate comparisons.
        if hasStroke and stroke <= self.vectorThreshold:
            stroke = 0
        else:
            stroke = int(stroke) # for GDI

        mygdi.SetMiterLimit(self.hDC, c_float(miterLimit), None)
        mygdi.SetPolyFillMode(self.hDC, ALTERNATE if fillmode == 'evenodd' else WINDING)

        # Backgrounds need to come first; if they don't they will result in GDI compositing everything so the background
        # covers up the stroke. This is more obvious with larger stroke widths.
        if fillcolor is not None and jobType != 'vector':
            brush = LOGBRUSH(BS_SOLID, fillcolor)
            hBrush = mygdi.CreateBrushIndirect(addressof(brush))
            mygdi.SelectObject(self.hDC, hBrush)
            emitter(pf if pf is not None else p, True)
            self.hasPrintedObjects = True

        # Older version of the extension gave everything strokes regardless of whether it had one or not
        if jobType == 'compatibility':
            if not hasStroke:
                stroke = 1
                hasStroke = True
            if color is None:
                color = 0

        # All stokes at or below the vector threshold are set to zero above
        if jobType == 'vector' and hasStroke and stroke > 0:
            hasStroke = False

        if color is not None and hasStroke and (jobType != 'raster' or stroke > 0):
            # Apologies to all who use this for things other than laser cutters. This skips handling line joints and
            # end caps properly for items which are going to get cut out as such details would be obliterated anyway.
            if stroke > 1:
                brush = LOGBRUSH(BS_SOLID, color, 0)
                hPen = mygdi.ExtCreatePen(PS_GEOMETRIC | PS_SOLID | strokeEndCap | strokeLineJoin, stroke, byref(brush), 0, None)
            else:
                hPen = mygdi.CreatePen(PS_SOLID, stroke, color)
            mygdi.SelectObject(self.hDC, hPen)
            emitter(p, False)
            self.hasPrintedObjects = True

    def emit_path(self, p, fill):
        mygdi.BeginPath(self.hDC)
        for sub in p:
            mygdi.MoveToEx(self.hDC, int(sub[0][1][0]), int(sub[0][1][1]), None)
            POINTS = c_long*(6*(len(sub)-1))
            points = POINTS()
            for i in range(len(sub)-1):
                points[6*i]     = int(sub[i][2][0])
                points[6*i + 1] = int(sub[i][2][1])
                points[6*i + 2] = int(sub[i + 1][0][0])
                points[6*i + 3] = int(sub[i + 1][0][1])
                points[6*i + 4] = int(sub[i + 1][1][0])
                points[6*i + 5] = int(sub[i + 1][1][1])
            mygdi.PolyBezierTo(self.hDC, addressof(points), 3*(len(sub)-1))
        mygdi.EndPath(self.hDC)

        if fill:
            mygdi.FillPath(self.hDC)
        else:
            mygdi.StrokePath(self.hDC)

    def emit_lines(self, p, fill):
        mygdi.BeginPath(self.hDC)
        for sub in p:
            mygdi.MoveToEx(self.hDC, int(sub[0][0][0]), int(sub[0][0][1]), None)
            for point in sub:
                mygdi.LineTo(self.hDC, int(point[0][0]), int(point[0][1]))
        mygdi.EndPath(self.hDC)

        if fill:
            mygdi.FillPath(self.hDC)
        else:
            mygdi.StrokePath(self.hDC)

    def print_useless_shape(self):
        # Delay is necessary, however small, or else it doesn't get handled correctly by GDI, apparently.
        mygdi.MoveToEx(self.hDC, 0, 0, None)
        sleep(.1)
        mygdi.LineTo(self.hDC, 0, 0)

    def fix_end_gaps(self, p):
        # What a mess. Turns out calling emit_polygon (mygdi.Polygon) multiple times or due to some other
        # mysterious circumstances results in polygons getting an extra wide outset "outline" instead of
        # a solid fill all the way across and then any other following polygon that may have requested
        # only drawing of an outline (but no fill) will have their center filled in. However if we just
        # call draw lines with all our existing points, the start and end points will have a gap between
        # them when the stroke is large. By inserting a new starting point halfway between the original
        # first two points we force the start and end to be straight with each other and avoid the gap!
        if len(p) >= 2:
            p = [[[(p[0][0][0]+p[1][0][0]) / 2, (p[0][0][1]+p[1][0][1]) / 2]]] + p[1:] + [p[0]]
        p.append([[p[0][0][0], p[0][0][1]]])
        return p

    def process_clone(self, node):
        fillmode = None
        style = node.get('style')
        if style:
            style = simplestyle.parseStyle(style)
            if style.has_key('display'):
                if style['display'] == 'none':
                    return
            if style.has_key('fill-rule'):
                fillmode = style['fill-rule']
        trans = node.get('transform')
        x = node.get('x')
        y = node.get('y')
        mat = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]
        if trans:
            mat = simpletransform.composeTransform(mat, simpletransform.parseTransform(trans))
        if x:
            mat = simpletransform.composeTransform(mat, [[1.0, 0.0, float(x)], [0.0, 1.0, 0.0]])
        if y:
            mat = simpletransform.composeTransform(mat, [[1.0, 0.0, 0.0], [0.0, 1.0, float(y)]])
        # push transform
        if trans or x or y:
            self.groupmat.append(simpletransform.composeTransform(self.groupmat[-1], mat))
        if fillmode:
            self.fillmode.append(fillmode)
        # get referenced node
        refid = node.get(inkex.addNS('href','xlink'))
        refnode = self.getElementById(refid[1:])
        if refnode is not None:
            if refnode.tag == inkex.addNS('g','svg'):
                self.process_group(refnode)
            elif refnode.tag == inkex.addNS('use', 'svg'):
                self.process_clone(refnode)
            else:
                self.process_shape(refnode, self.groupmat[-1], self.fillmode[-1])
        # pop transform
        if trans or x or y:
            self.groupmat.pop()
        if fillmode:
            self.fillmode.pop()

    def process_group(self, group):
        fillmode = None
        style = group.get('style')
        if style:
            style = simplestyle.parseStyle(style)
            if group.get(inkex.addNS('groupmode', 'inkscape')) == 'layer' and style.has_key('display'):
                if style['display'] == 'none' and self.visibleLayers:
                    return
                if style.has_key('fill-rule'):
                    fillmode = style['fill-rule']
            elif style.has_key('display') and style['display'] == 'none':
                return
        trans = group.get('transform')
        if trans:
            self.groupmat.append(simpletransform.composeTransform(self.groupmat[-1], simpletransform.parseTransform(trans)))
        if fillmode:
            self.fillmode.append(fillmode)
        for node in group:
            if node.tag == inkex.addNS('g','svg'):
                self.process_group(node)
            elif node.tag == inkex.addNS('use', 'svg'):
                self.process_clone(node)
            else:
                self.process_shape(node, self.groupmat[-1], self.fillmode[-1])
        if trans:
            self.groupmat.pop()
        if fillmode:
            self.fillmode.pop()

    def effect(self):
        self.hasPrintedObjects = False
        doc = self.document.getroot()

        if doc.get('preserveAspectRatio') != None:
            exit(_('This extension does not support the SVG attribute "preserveAspectRatio"'))

        # Flags (in order of declaration):
        #    Use the 'printer setup' version of the print dialog
        #    Return hDC (device context)
        #    Hide the network button
        printdlg = PRINTDLG(PD_PRINTSETUP | PD_RETURNDC | PD_NONETWORKBUTTON)

        # get printer properties dialog
        if not mycomdlg.PrintDlgA(printdlg):
            error = mycomdlg.CommDlgExtendedError()
            if int(error) in printErrors:
                exit(_(str(error) + ' ' + printErrors[int(error)]))
                pass
            elif int(error) > 0:
                exit(_('Unknown Windows error returned'))
                pass
            else:
                exit()

        # Places online said to do this. It's apparently important. Prevents them from moving
        # around in memory while we we're trying to use them or something...? :P
        modeaddr = mykernel.GlobalLock(printdlg.hDevMode)
        nameaddr = mykernel.GlobalLock(printdlg.hDevNames)

        devmode = cast(modeaddr, POINTER(DEVMODE))
        devnames = cast(nameaddr, POINTER(DEVNAMES))

        pquality = devmode[0].dmPrintQuality
        pscale = devmode[0].dmScale
        pname = c_char_p(nameaddr + devnames[0].wDeviceOffset)

        # Look for text objects

        flows = doc.xpath("//svg:flowRoot", namespaces=inkex.NSS)
        text = doc.xpath("//svg:text", namespaces=inkex.NSS)
        if ((flows is not None and len(flows) > 0) or (text is not None and len(text) > 0)) and self.options.autoConvertText == 'true':

            # Via Alvin Penner; Inkscape pops a dialog if it can't detect the file format so a copy with an extension fixes this
            file = self.args[-1]
            tempfile = file + "-print_Win32_vector.svg"
            copy2(file, tempfile)

            operations = ''
            if flows is not None and len(flows) > 0:
                operations += '--select=' + flows[0].get('id') + ' --verb=EditSelectSameObjectType --verb=ObjectToPath '

            if text is not None and len(text) > 0:
                operations += '--select=' + text[0].get('id') + ' --verb=EditSelectSameObjectType --verb=ObjectToPath '

            process = Popen('inkscape ' + operations + '--verb=FileSave --verb=FileQuit -f "' + tempfile + '"',
                shell=True, stdout=PIPE, stderr=PIPE)
            stdout, stderr = process.communicate()
            if stdout:
                inkex.debug(stdout)
            if stderr:
                try:
                    os.remove(tempfile)
                except OSError:
                    pass
                exit(stderr)

            try:
                stream = open(tempfile, 'r')
            except IOError:
                errormsg(_('Unable to open pathified temp file copy: %s') % tempfile)
                try:
                    os.remove(tempfile)
                except OSError:
                    pass
                exit()

            parser = etree.XMLParser(huge_tree=True)
            doc = etree.parse(stream, parser=parser).getroot()
            stream.close()
            os.remove(tempfile)

        # initialize print document

        docname = doc.xpath('@sodipodi:docname', namespaces=inkex.NSS)
        if not docname:
            docname = ['New document 1']
        docInfo = DOCINFO(docname[0].split('\\')[-1])
        self.hDC = mygdi.CreateDCA(None, pname, None, byref(devmode[0]))
        if mygdi.StartDocA(self.hDC, byref(docInfo)) < 0:
            exit()                      # user clicked Cancel

        self.scale = pquality/96.0        # use PrintQuality from DEVMODE
        self.scale /= self.unittouu('1px')

        # make comparisons after having applied all transforms easier
        self.vectorThreshold = self.options.vectorThreshold * self.scale

        # process viewBox height attribute to correct page scaling
        viewBox = doc.get('viewBox')
        if viewBox:
            viewBox2 = viewBox.split(',')
            if len(viewBox2) < 4:
                viewBox2 = viewBox.split(' ')
            self.scale *= self.unittouu(doc.get('height')) / self.unittouu(self.addDocumentUnit(viewBox2[3]))
        self.groupmat = [[[self.scale, 0.0, 0.0], [0.0, self.scale, 0.0]]]

        self.fillmode = ['nonzero']
        self.process_group(doc)

        # The Epilog Mini 24 doesn't like it when it gets nothing and hangs
        # This prints an object with no outlines or fills which when run ends immediately without doing anything
        if not self.hasPrintedObjects:
            self.print_useless_shape()
        mygdi.EndDoc(self.hDC)

        # We locked it, we gotta unlock it. So says the all mighty MSDN.
        mykernel.GlobalUnlock(printdlg.hDevNames)
        mykernel.GlobalUnlock(printdlg.hDevMode)

if __name__ == '__main__':
    e = Win32VectorPrinter()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
